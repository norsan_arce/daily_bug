<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Bugs;
use \App\Categories;
use Auth;
use \App\Solutions;

class BugsController extends Controller
{
    public function index(){
    	$bugs = Bugs::all();

    	return view('/bugs',compact('bugs'));
    }
    public function showAddBug(){
    	$categories = Categories::all();
    	return view('userviews.add-bug', compact('categories'));
    }


    public function store(Request $req) {
    	$newBug = new Bugs;

    	$newBug->title = $req->title;
    	$newBug->body = $req->body;
    	$newBug->category_id = $req->category_id;
    	$newBug->status_id =1;

    	$newBug->user_id = Auth::user()->id;

    	$newBug->save();

    	return redirect('/allbugs');
    }

    public function indivBugs(){

    	$bugs = Bugs::where('user_id', Auth::user()->id)->get();

    	return view('userviews.mybugs', compact("bugs"));
    	#SELECT * FROM bugs where user_id = Auth::iser()->id;
    }

    public function destroy($id){
    	$bugToDelete = Bugs::find($id);
    	$bugToDelete->Delete();
    	return redirect('/mybugs');
    }

     public function deleteSolution($id){
        $toDelete = Solutions::find($id);
        $toDelete->Delete();

        return redirect()->back();
    }

    public function edit($id){

    	$bug = Bugs::find($id);
    	$categories = Categories::all();

    	return view('userviews.editbug', compact('bug','categories'));
    }

    public function update($id, Request $req){
    	$bugToEdit = Bugs::find($id);

    	$bugToEdit->title = $req->title;
    	$bugToEdit->body = $req->body;
    	$bugToEdit->category_id = $req->category_id;
    	$bugToEdit->save();

    	return redirect('mybugs');
    }

    public function showSolve($id){
    	$bug = Bugs::find($id);

    	return view('adminviews.solveform', compact('bug'));
    }

    public function saveSolution(Request $req){
    	$newSolution = new Solutions;
    	$newSolution->title = $req->title;
    	$newSolution->body = $req->body;
    	$newSolution->bug_id = $req->bug_id;
    	$newSolution->status_id = 5;
    	$newSolution->save();

    	#for now, redirect to all bugs
    	// return redirect('allbugs');

        $newId = $newSolution->id;

        $bug =Bugs::find($req->bug_id);
        $solutions = Solutions::where('bug_id',$req->bug_id)->get();

        // return redirect('/indivbug/' . $newId, compact('bug','solutions'));

        return redirect('/bugs');


    }

    public function showIndivBug($id){

        $bug = Bugs::find($id);
        $solutions = Solutions::where("bug_id", $id)->get();


        return view('indivbug',compact('bug','solutions'));
    }


}


