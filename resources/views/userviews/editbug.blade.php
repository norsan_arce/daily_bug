@extends("layouts.app")
@section("content")

<h1 class="text-center py-5">Edit Bug</h1>

<article class="col-lg-6 offset-3">
	<form action="/editbug/{{$bug->id}}" method="POST">
		@csrf
		@method('PATCH')
		<section class="form-group">
			<label for="title">Bug Title</label>
			<input type="text" name="title" class="form-control" value="{{$bug->title}}">
		</section>
		<section class="form-group">
			<label for="body">Bug Description</label>
			<input type="text" name="body" class="form-control" value="={{$bug->body}}">
		</section>
		<section class="form-group">
			<label for="category">Category</label>
			<select name="category_id" class="form-control">
				@foreach($categories as $indiv_category);
				<option value="{{$indiv_category->id}}"
				{{$bug->category_id == $indiv_category->id ? "selected" : ""}}

					>{{$indiv_category->name}}
				</option>
				@endforeach
			</select>
		</section>
		<button class="btn btn-success" type="submit">Edit</button>
	</form>
</article>
@endsection