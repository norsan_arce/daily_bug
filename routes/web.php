<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', function () {
    return view('dailybuglanding');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

#For all
#for all bugs
Route::get('/allbugs','BugsController@index');

Route::get('/indivbug/{id}','BugsController@showIndivBug');

#TRIAL
Route::delete('/deletesolution/{id}','BugsController@deleteSolution');

#User Routes
#To show add bug form
Route::get('/addbugs', 'BugsController@showAddBug');

#To save new bug

Route::post('/addbugs', 'BugsController@store');

#To show user bugs

Route::get('/mybugs','BugsController@indivBugs');

#to delete bugs
Route::delete('/deletebug/{id}', 'BugsController@destroy');

#to go the edit form
Route::get('/editbug/{id}', 'BugsController@edit');

#to save edited bug

Route::patch('editbug/{id}', 'BugsController@update');

// Admin Routes
// Togo to solve form
Route::get('/solve/{id}', 'BugsController@showSolve');

# To save
Route::post('/solve', 'BugsController@saveSolution');